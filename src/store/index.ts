import Vue from "vue";
import Vuex from "vuex";

import example from "./modules/example.module";
import user from "./modules/user.module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    example,
    user
  }
});
