import { User } from "@/services/user/entities/user.interface";

export interface UserState {
  user?: User;
  userRecord?: { winned: number; losed: number; bestScore: number };
  ranking?: { alias: string; puntaje: number }[];
}
