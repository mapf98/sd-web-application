import Vue from "vue";
import { UserState } from "./states/user.state";
import UserService from "../../services/user/user.service";
import { User } from "@/services/user/entities/user.interface";

export default {
  namespaced: true,
  state: {
    user: {
      alias: "",
      first_name: "",
      last_name: "",
      email: "",
      gender: ""
    },
    userRecord: {
      winned: 0,
      losed: 0,
      bestScore: 0
    },
    ranking: []
  },
  getters: {
    getUserInfo: (state: UserState) => state.user,
    getUserGamesRecord: (state: UserState) => state.userRecord,
    getRanking: (state: UserState) => state.ranking
  },
  mutations: {
    setUserInfo(state: UserState, user: User) {
      Vue.set(state, "user", user);
    },
    setUserGamesRecord(state: UserState, userRecord: any) {
      Vue.set(state, "userRecord", userRecord);
    },
    setRanking(state: UserState, ranking: any) {
      Vue.set(state, "ranking", ranking);
    }
  },
  actions: {
    signUp: async (context: any, payload: User) => {
      const response = await UserService.signUp(payload);
      if (response.data.error) {
        return { error: true, message: response.data.error };
      } else {
        return { error: false, message: "" };
      }
    },
    logIn: async (context: any, payload: any) => {
      const response = await UserService.logIn(payload);
      const info = {
        token: response.data.access_token,
        alias: payload.user.username
      };
      localStorage.setItem("info", JSON.stringify(info));

      if (response.data.error) {
        return { error: true, message: response.data.error };
      } else {
        return { error: false, message: "" };
      }
    },
    getUserInfo: async (context: any) => {
      const response = await UserService.getUserInfo();
      context.commit("setUserInfo", response.data);
    },
    getUserGamesRecord: async (context: any) => {
      const games = await UserService.getUserGames();
      const bestScore = await UserService.getUserBestScore();
      context.commit("setUserGamesRecord", {
        winned: games.data.ganados,
        losed: games.data.perdidas,
        bestScore: bestScore.data[0].record
      });
    },
    getRanking: async (context: any) => {
      const response = await UserService.getRanking();
      context.commit("setRanking", response.data);
    }
  }
};
