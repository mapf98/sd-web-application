import Vue from "vue";
import { ExampleState } from "./states/example.state";

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {}
};
