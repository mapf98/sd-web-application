import { API_URL } from "../service.config";
import { User } from "./entities/user.interface";
import info from "../../utils/info.util";

export default {
  signUp(user: User) {
    return API_URL.post(`/register`, { user: user });
  },
  logIn(user: any) {
    return API_URL.post(`/login`, user);
  },
  getUserInfo() {
    return API_URL.get(`/users/info/${info.getAlias()}`, {
      headers: {
        Authorization: `Bearer ${info.getToken()}`
      }
    });
  },
  getUserGames() {
    return API_URL.get(`/users/games/${info.getAlias()}`, {
      headers: {
        Authorization: `Bearer ${info.getToken()}`
      }
    });
  },
  getUserBestScore() {
    return API_URL.get(`/users/record/${info.getAlias()}`, {
      headers: {
        Authorization: `Bearer ${info.getToken()}`
      }
    });
  },
  getRanking() {
    return API_URL.get(`/users/ranking`, {
      headers: {
        Authorization: `Bearer ${info.getToken()}`
      }
    });
  }
};
