import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Login from "../views/Login.vue";
import Signup from "../views/Signup.vue";
import Rankings from "../views/Rankings.vue";
import info from "../utils/info.util";
import { VueEasyJwt } from "vue-easy-jwt";
const jwt = new VueEasyJwt();

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      auth: false
    }
  },
  {
    path: "/signup",
    name: "Signup",
    component: Signup,
    meta: {
      auth: false
    }
  },
  {
    path: "/rankings",
    name: "Rankings",
    component: Rankings,
    meta: {
      auth: true
    }
  },
  {
    path: "/",
    redirect: "/login"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  to.matched.some(route => {
    if (info.getToken() !== undefined) {
      const token: string = info.getToken();
      if (jwt.isExpired(token)) {
        localStorage.removeItem("info");
        next({ path: "/login" });
      } else {
        next();
      }
    } else {
      if (route.path == "/login" || route.path == "/signup") {
        next();
      } else {
        next({ path: "/login" });
      }
    }

    if (route.path == "/login") {
      if (info.getToken() !== undefined) {
        const token: string = info.getToken();
        if (jwt.isExpired(token)) {
          localStorage.removeItem("info");
          next();
        } else {
          next({ path: "/rankings" });
        }
      } else {
        next();
      }
    }
  });
});

export default router;
